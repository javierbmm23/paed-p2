const loadJSON = require('../utilities/load_data');
const percentage = require('../utilities/percentage');

var dataset = loadJSON.loader("../datasets/dataset150.json");
var minCal = 1000;
var maxCal = 3000;
var minFatPercentage = 20;
var maxFatPercentage = 35;
var final_menu = [];
var bestFatPerc = Infinity;
var bestCalories = -1;
var totalCal = 0;
var totalFat = 0;

const list_json = function(){
    var dataset = loadJSON.loader("../datasets/dataset100.json");

    for( let object of dataset)
        console.log(object);
};

const listMenu = function(menu){
    console.log("****************************************\n MENU IS READY!");
    for(let dish of menu){
        console.log(dish);
    }
};

const getFatPercentage = function(dish){
    let energetic_value = dish['energetic_value'];

    return percentage.getPercentage(energetic_value, dish.fat);
};

const insideMaxLimit = function(){
    return (totalCal <= maxCal && percentage.getPercentage(totalCal, totalFat) <= maxFatPercentage);
};

const insideMinLimit = function(){
    return (totalCal >= minCal && percentage.getPercentage(totalCal, totalFat) >= minFatPercentage);
};

const insideLimit = function(menu){
    return insideMinLimit(menu) && insideMaxLimit(menu);
};

const isBest = function(menu){
    return (percentage.getPercentage(totalCal, totalFat)  < bestFatPerc && totalCal > bestCalories);
};
const isWorse = function(menu){
    return (percentage.getPercentage(totalCal, totalFat)  > bestFatPerc);
};
const updateBest = function(menu){
    bestFatPerc = percentage.getPercentage(totalCal, totalFat) ;
    bestCalories = totalCal;
    final_menu = menu;
};

const getMenuUtil = function(dataset, current, numOfDishes, menu){
    let length = dataset.length;
    while(current < length){
        menu.push(dataset[current]);    // Adding element (dish) to menu
        totalFat += dataset[current].fat;
        totalCal += dataset[current].energetic_value;

        if(insideMaxLimit(menu)){  // Checking maximum limit values
            if(menu.length === numOfDishes){ // If Menu is ready
                if(isBest(menu) && insideMinLimit()){           // Check best
                    updateBest(menu);
                    listMenu(menu);
                    console.log("fat:" + bestFatPerc);
                    console.log("Calories:" + bestCalories);
                }
            }else{
                if(isWorse(menu)){  // this configuration is already worse, so let's cut it out
                    //console.log("worse!\n");
                }else{  // feasible
                    getMenuUtil(dataset, current+1, numOfDishes, menu);
                }
            }
        }
        // prepare for next configuration:
        menu.pop();
        totalFat -= dataset[current].fat;
        totalCal -= dataset[current].energetic_value;
        current++;
    }

    return;
};

const getMenu = function(){
    let current = 0;
    let menu = [];
    let numOfDishes = 5;

    getMenuUtil(dataset, current, numOfDishes, menu);
};
getMenu();

