module.exports.getPercentage = function(value, fraction){
    let percentage = (fraction/value)*100;
    return percentage;
};